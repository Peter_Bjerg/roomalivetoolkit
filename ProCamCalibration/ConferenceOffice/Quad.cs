﻿using System;
using System.Security.Cryptography;
using RoomAliveToolkit;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.WIC;
using Matrix = SharpDX.Matrix;

namespace ConferenceOffice
{
    public class Quad
    {
        private static float HALF_WIDTH = 2f / 2;
        private static float HALF_HEIGHT = 2f / 2;
        private static float ROTATION_FACTOR = 90f/2.5f;

        private MeshShader meshShader;
        private MeshDeviceResources meshDeviceResources;
        private RenderTargetView userViewRenderTargetView;
        private DepthStencilView userViewDepthStencilView;
        private Viewport userViewViewport;
        private PointLight pointLight;
        private SharpDX.Matrix viewProjection;
        private DeviceContext deviceContext;
        private float xOffset = 0;
        private float yOffset = 0;

        public float rotationAngel;

        private ShaderResourceView desktopTextureSRV;
        private int color;

        private SharpDX.Matrix baseWorld;
        private SharpDX.Matrix currentWorldWithRotation;

        public Quad(MeshShader meshShader, MeshDeviceResources meshDeviceResources, RenderTargetView renderTargetView, DepthStencilView depthStencilView, Viewport viewport, SharpDX.Matrix baseWorld, int color)
        {
            this.meshShader = meshShader;
            this.meshDeviceResources = meshDeviceResources;
            this.userViewRenderTargetView = renderTargetView;
            this.userViewDepthStencilView = depthStencilView;
            this.userViewViewport = viewport;
            this.baseWorld = baseWorld;
            this.currentWorldWithRotation = baseWorld;
            this.color = color;

            pointLight = new PointLight();
            pointLight.position = new Vector3(0, 1, 0);
            pointLight.Ia = new Vector3(0.1f, 0.1f, 0.1f);
        }


        public Quad(MeshShader meshShader, MeshDeviceResources meshDeviceResources, RenderTargetView renderTargetView, DepthStencilView depthStencilView, Viewport viewport, SharpDX.Matrix baseWorld, ShaderResourceView textureSRV)
        {
            this.meshShader = meshShader;
            this.meshDeviceResources = meshDeviceResources;
            this.userViewRenderTargetView = renderTargetView;
            this.userViewDepthStencilView = depthStencilView;
            this.userViewViewport = viewport;
            this.baseWorld = baseWorld;
            this.currentWorldWithRotation = baseWorld;
            this.desktopTextureSRV = textureSRV;

            pointLight = new PointLight();
            pointLight.position = new Vector3(0, 1, 0);
            pointLight.Ia = new Vector3(0.1f, 0.1f, 0.1f);
        }


        public void SetChangingVariables(SharpDX.Direct3D11.DeviceContext deviceContext, SharpDX.Matrix viewProjection)
        {
            this.deviceContext = deviceContext;
            this.viewProjection = viewProjection;
        }

        public void SetChangingVariables(SharpDX.Direct3D11.DeviceContext deviceContext, SharpDX.Matrix viewProjection, SharpDX.Matrix translation)
        {
            this.SetChangingVariables(deviceContext, viewProjection);
            if (Office.scaledDemo)
            {
                CalculatePureTranslation(translation);
            } else if (Office.cylinderDemo)
            {
                CalculateRotation(translation);
            }

        }

        public void CalculateRotation(SharpDX.Matrix translation)
        {
            
            var x = translation.TranslationVector.X;
            var y = translation.TranslationVector.Y;

            var newTranslation = SharpDX.Matrix.Translation(0, y + yOffset, 1.8f);
            rotationAngel = (x*ROTATION_FACTOR/180f*(float) Math.PI) + xOffset;

            var nintyDegreeRads = (float) Math.PI/2;

            if (rotationAngel > nintyDegreeRads)
            {
                rotationAngel = nintyDegreeRads;
            } else if (rotationAngel < -nintyDegreeRads)
            {
                rotationAngel = -nintyDegreeRads;
            } 
            var rotationY = SharpDX.Matrix.RotationY(rotationAngel);

            var cylinderPlacement = newTranslation*rotationY;

            this.currentWorldWithRotation = Matrix.Scaling(baseWorld.ScaleVector) * cylinderPlacement * Matrix.Translation(baseWorld.TranslationVector);
            Console.WriteLine(rotationAngel);
        }

        public void CalculatePureTranslation(SharpDX.Matrix translation)
        {
            var x = translation.TranslationVector.X;
            var y = translation.TranslationVector.Y;

            var newTranslation = SharpDX.Matrix.Translation(x + xOffset, y + yOffset, baseWorld.TranslationVector.Z);

            this.currentWorldWithRotation = SharpDX.Matrix.Scaling(baseWorld.ScaleVector) * newTranslation;
        }

        public bool Collision(Matrix cursorTranslation, int debug, float cursorUnrotatedX)
        {
            if (Office.scaledDemo)
            {
                return CollisionScaled(cursorTranslation, debug);
            }
            else if (Office.cylinderDemo)
            {
                return CollisionCylinder(cursorTranslation, debug, cursorUnrotatedX);
            }
            return false;
        }

        public bool CollisionScaled(Matrix cursorTranslate, int debug)
        {
            var x = cursorTranslate.TranslationVector.X;
            var y = cursorTranslate.TranslationVector.Y;

            //Calculating distance from center of quad (translation.X) to left and right edge
            float leftX = (currentWorldWithRotation.TranslationVector.X + (HALF_WIDTH * currentWorldWithRotation.ScaleVector.X));
            float rightX = (currentWorldWithRotation.TranslationVector.X - (HALF_WIDTH * currentWorldWithRotation.ScaleVector.X));

            //Calculating distance from center of quad (translation.Y) to upper and lower edge
            float upperY = currentWorldWithRotation.TranslationVector.Y + HALF_HEIGHT * currentWorldWithRotation.ScaleVector.Y;
            float lowerY = currentWorldWithRotation.TranslationVector.Y - HALF_HEIGHT * currentWorldWithRotation.ScaleVector.Y;

            var collision = leftX > x && rightX < x && lowerY < y && upperY > y;

            if (debug == 0 && collision)
            {
                xOffset = currentWorldWithRotation.TranslationVector.X - x;
                yOffset = currentWorldWithRotation.TranslationVector.Y - y;
            }

            return collision;
        }

        public bool CollisionCylinder(Matrix cursorTranslate, int debug, float cursorRotationAngel)
        {
            var adjacent = 1.6f;
            var opposite = baseWorld.ScaleVector.X;

            var angel = Math.Atan(opposite / adjacent);

            float leftX = rotationAngel + (float)angel;
            float rightX = rotationAngel - (float)angel;

            var y = cursorTranslate.TranslationVector.Y;

            //Calculating distance from center of quad (translation.Y) to upper and lower edge
            float upperY = currentWorldWithRotation.TranslationVector.Y + HALF_HEIGHT * currentWorldWithRotation.ScaleVector.Y;
            float lowerY = currentWorldWithRotation.TranslationVector.Y - HALF_HEIGHT * currentWorldWithRotation.ScaleVector.Y;

            var collision = leftX > cursorRotationAngel && rightX < cursorRotationAngel && lowerY < y && upperY > y;

            if (debug == 0 && collision)
            {
                xOffset = rotationAngel - cursorRotationAngel;
                yOffset = currentWorldWithRotation.TranslationVector.Y - y;
            }

            return collision;
        }

        public void Render()
        {
            meshShader.SetVertexShaderConstants(deviceContext, currentWorldWithRotation, viewProjection, pointLight.position);
            if (desktopTextureSRV != null)
            {
                meshShader.WindowRender(deviceContext, meshDeviceResources, pointLight, userViewRenderTargetView,
                    userViewDepthStencilView, userViewViewport, desktopTextureSRV, 0);
            }
            else
            {
                meshShader.WindowRender(deviceContext, meshDeviceResources, pointLight, userViewRenderTargetView,
                    userViewDepthStencilView, userViewViewport, null, color);
            }
        }

        /// <summary>
        /// Gives the quad a color based on a number. 0 is Red, 1 is Green, 2 is Yellow, 3 is Blue
        /// </summary>
        /// <param name="color">Number representing the color</param>
        public void RenderDebug(int color)
        {
            meshShader.SetVertexShaderConstants(deviceContext, currentWorldWithRotation, viewProjection, pointLight.position);
            meshShader.WindowRender(deviceContext, meshDeviceResources, pointLight, userViewRenderTargetView,
                userViewDepthStencilView, userViewViewport, null, color);
        }
    }
}