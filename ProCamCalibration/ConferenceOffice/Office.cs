﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Microsoft.Kinect;
using RoomAliveToolkit;
using SharpDX;
using SharpDX.Direct3D11;
using SharpDX.DXGI;
using SharpDX.WIC;
using Matrix = SharpDX.Matrix;

namespace ConferenceOffice
{
    public class Office : ApplicationContext
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application.Run(new Office());
        }

        OutputDuplication outputDuplication;

        const int userViewTextureWidth = 10000;
        const int userViewTextureHeight = 1000;
        List<ProjectorForm> projectorForms = new List<ProjectorForm>();
        //        DepthAndColorShader depthAndColorShader;
        ProjectiveTexturingShader projectiveTexturingShader;
        Dictionary<ProjectorCameraEnsemble.Camera, ProjectionMappingSample.CameraDeviceResource> cameraDeviceResources = new Dictionary<ProjectorCameraEnsemble.Camera, ProjectionMappingSample.CameraDeviceResource>();
        Object renderLock = new Object();
        RenderTargetView userViewRenderTargetView/*, filteredUserViewRenderTargetView*/;
        DepthStencilView userViewDepthStencilView;
        ShaderResourceView userViewSRV, /*filteredUserViewSRV,*/ desktopTextureSRV, desktopTextureSRV2;
        Viewport userViewViewport;
        SharpDX.Direct3D11.Device device;
        ProjectorCameraEnsemble ensemble;
        Form1 userViewForm;
        MeshShader meshShader;
        MeshDeviceResources meshDeviceResources;
        PassThrough passThroughShader;
        //        RadialWobble radialWobbleShader;
        FromUIntPS fromUIntPS;
        BilateralFilter bilateralFilter;
        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        //        PointLight pointLight = new PointLight();
        Texture2D desktopTexture, desktopTexture2;

        private bool threeDObjectEnabled = true;
        private bool desktopDuplicationEnabled = false;
        private bool doubleDesktopDuplicationEnabled = false;
        private bool KinectTrackingEnabled = localHeadTracking || handTracking;
        private bool fullScreenEnabled = true;
        private static bool localHeadTracking = false;
        private static bool handTracking = true;

        public static bool scaledDemo = false;
        public static bool cylinderDemo = true;

        IntPtr windowPtr, windowPtr2;
        const int leftNudge = 9;
        const int topNudge = -10;
        const int rightNudge = 0;
        const int bottomNudge = 10;

        SharpDX.WIC.ImagingFactory2 imagingFactory = new ImagingFactory2();
        float alpha = 1;

        KinectSensor localKinectSensor;
        BodyFrameReader bodyFrameReader;
        Body[] bodies = null;
        CameraSpacePoint headCameraSpacePoint, handLeftCameraSpacePoint, handRightCameraSpacePoint,
            handLeftTipCameraSpacePoint, handRightTipCameraSpacePoint, thumbLeftCameraSpacePoint, thumbRightCameraSpacePoint,
            elbowLeftCameraSpacePoint, elbowRightCameraSpacePoint;
        Object headCameraSpacePointLock = new Object();
        bool trackingValid = false;

        private List<Quad> quads = new List<Quad>();
        private Quad cursor;

        private int debug = 0;
        SharpDX.Vector3 headPositionDefault = new SharpDX.Vector3(0.3f, 0.2f, 0f); // may need to change this default
        SharpDX.Vector3 handRightPositionDefault = new SharpDX.Vector3(0, 0, 0);
        public Office()
        {
            // load ensemble.xml
            string path = "C:\\Users\\UBI PC\\Desktop\\Peter\\2kinects\\2kinects.xml";
            string directory = Path.GetDirectoryName(path);
            ensemble = RoomAliveToolkit.ProjectorCameraEnsemble.FromFile(path);

            // create d3d device
            var factory = new Factory1();
            var adapter = factory.Adapters[0];

            // When using DeviceCreationFlags.Debug on Windows 10, ensure that "Graphics Tools" are installed via Settings/System/Apps & features/Manage optional features.
            // Also, when debugging in VS, "Enable native code debugging" must be selected on the project.
            device = new SharpDX.Direct3D11.Device(adapter, DeviceCreationFlags.None);

            // shaders
            CreateShaders();

            // one user view
            // user view render target, depth buffer, viewport for user view
            CreateUserView();

            // user view depth buffer
            CreateUserDepth();

            // create device objects for each camera
            foreach (var camera in ensemble.cameras)
                cameraDeviceResources[camera] = new ProjectionMappingSample.CameraDeviceResource(device, camera, renderLock, directory);


            // user view viewport
            userViewViewport = new Viewport(0, 0, userViewTextureWidth, userViewTextureHeight, 0f, 1f);

            if (desktopDuplicationEnabled)
            {
                SetupDesktopDuplicate();
            }

            if (doubleDesktopDuplicationEnabled)
            {
                SetupDesktopDuplicate2();
            }

            // desktop duplication
            var output = new Output1(factory.Adapters[0].Outputs[0].NativePointer); // TODO: change adapter and output number
            outputDuplication = output.DuplicateOutput(device);

            // example 3d object
            if (scaledDemo)
            {
                CreateQuadsScaled();
            }
            else if (cylinderDemo)
            {
                CreateQuadsCylinder();
            }



            // create a form for each projector
            foreach (var projector in ensemble.projectors)
            {
                var form = new ProjectorForm(factory, device, renderLock, projector);
                if (fullScreenEnabled)
                    form.FullScreen = fullScreenEnabled; // TODO: fix this so can be called after Show
                form.Show();
                projectorForms.Add(form);
            }

            userViewForm = new OfficeForm(factory, device, renderLock);
            userViewForm.Show();

            if (KinectTrackingEnabled)
            {
                localKinectSensor = KinectSensor.GetDefault();
                bodyFrameReader = localKinectSensor.BodyFrameSource.OpenReader();
                localKinectSensor.Open();
                Console.WriteLine("connected to local camera");
                new System.Threading.Thread(LocalBodyLoop).Start();
            }

            new System.Threading.Thread(RenderLoop).Start();

        }

        void RenderLoop()
        {
            while (true)
            {
                lock (renderLock)
                {
                    var deviceContext = device.ImmediateContext;

                    // render user view
                    deviceContext.ClearRenderTargetView(userViewRenderTargetView, new Color4(0f, 0f, 0f, 0f));
                    deviceContext.ClearDepthStencilView(userViewDepthStencilView, DepthStencilClearFlags.Depth, 1, 0);

                    SharpDX.Vector3 headPosition = headPositionDefault;
                    SharpDX.Vector3 handRightPosition = new Vector3();

                    if (KinectTrackingEnabled)
                    {
                        if (localHeadTracking)
                        {
                            headPosition = TrackedHeadPosition();
                        }
                        if (handTracking)
                        {
                            handRightPosition = TrackedHandPosition();
                        }
                    }
                    var userView = GraphicsTransforms.LookAt(headPosition, headPosition + SharpDX.Vector3.UnitZ, SharpDX.Vector3.UnitY);
                    userView.Transpose();


                    //Console.WriteLine("headPosition = " + headPosition);


                    float aspect = (float)userViewTextureWidth / (float)userViewTextureHeight;
                    //More is more
                    var userFormProjection = GraphicsTransforms.PerspectiveFov(90.0f / 180.0f * (float)Math.PI, aspect, 0.001f, 1000.0f);
                    userFormProjection.Transpose();

                    //Less is more
                    //                    var projectorFormProjection = GraphicsTransforms.PerspectiveFov(60.0f / 180.0f * (float)Math.PI, aspect, 0.001f, 1000.0f);
                    //                    projectorFormProjection.Transpose();
                    //                    projectorFormProjection = userFormProjection;

                    // smooth depth images
                    foreach (var camera in ensemble.cameras)
                    {
                        SmoothDepthImage(camera, deviceContext);
                    }

                    // desktop duplication
                    if (desktopDuplicationEnabled)
                    {
                        var outputDuplicateFrameInformation = default(OutputDuplicateFrameInformation);
                        SharpDX.DXGI.Resource resource = null;
                        outputDuplication.AcquireNextFrame(1000, out outputDuplicateFrameInformation, out resource);
                        var texture = resource.QueryInterface<Texture2D>();


                        var rect = new ProjectionMappingSample.RECT();
                        ProjectionMappingSample.GetWindowRect(windowPtr, out rect);

                        var sourceRegion = new ResourceRegion()
                        {
                            Left = rect.Left + leftNudge,
                            Right = rect.Right + rightNudge,
                            Top = rect.Top + topNudge,
                            Bottom = rect.Bottom + bottomNudge,
                            Front = 0,
                            Back = 1,
                        };
                        deviceContext.CopySubresourceRegion(texture, 0, sourceRegion, desktopTexture, 0);
                        if (!doubleDesktopDuplicationEnabled)
                        {
                            texture.Dispose();
                        }
                        if (desktopDuplicationEnabled && doubleDesktopDuplicationEnabled)
                        {
                            ProjectionMappingSample.GetWindowRect(windowPtr2, out rect);

                            sourceRegion = new ResourceRegion()
                            {
                                Left = rect.Left + leftNudge,
                                Right = rect.Right + rightNudge,
                                Top = rect.Top + topNudge,
                                Bottom = rect.Bottom + bottomNudge,
                                Front = 0,
                                Back = 1,
                            };
                            deviceContext.CopySubresourceRegion(texture, 0, sourceRegion, desktopTexture2, 0);
                            texture.Dispose();
                        }

                    }

                    // 3d object
                    if (threeDObjectEnabled)
                    {
                        var viewProjection = userView * userFormProjection;

                        var cursorColor = 2;
                        var cursorCollision = false;

                        var cursorTranslation = SharpDX.Matrix.Translation(handRightPosition.X * 3, handRightPosition.Y * 3, 0);
                        cursor.SetChangingVariables(deviceContext, viewProjection, cursorTranslation);
                        //                        cursor.Render();

                        for (int i = 0; i < quads.Count; i++)
                        {
                            var quad = quads[i];
                            if (quad.Collision(cursorTranslation, debug, cursor.rotationAngel))
                            {
                                cursorColor = 1;
                                if (debug == 1)
                                {
                                    cursorColor = 0;
                                    quad.SetChangingVariables(deviceContext, viewProjection, cursorTranslation);
                                    quad.Render();
                                }
                                else
                                {
                                    quad.SetChangingVariables(deviceContext, viewProjection);
                                    quad.Render();
                                }
                            }
                            else
                            {
                                quad.SetChangingVariables(deviceContext, viewProjection);
                                quad.Render();
                            }
                        }

                        cursor.RenderDebug(cursorColor);
                    }

                    // render user view to seperate form
                    //                    passThroughShader.viewport = new Viewport(userViewForm.Width / 2, 0, userViewForm.Width, userViewForm.Height);
                    passThroughShader.viewport = new Viewport(0, 0, userViewForm.Width, userViewForm.Height);
                    // TODO: clean this up by simply using a pointer to the userViewSRV
                    if (threeDObjectEnabled)
                    {
                        passThroughShader.Render(deviceContext, userViewSRV, userViewForm.renderTargetView);
                    }
                    userViewForm.swapChain.Present(0, PresentFlags.None);

                    // projection puts x and y in [-1,1]; adjust to obtain texture coordinates [0,1]
                    // TODO: put this in SetContants?
                    userFormProjection[0, 0] /= 2;
                    userFormProjection[1, 1] /= -2; // y points down
                    userFormProjection[2, 0] += 0.5f;
                    userFormProjection[2, 1] += 0.5f;

                    // projection mapping for each projector
                    foreach (var form in projectorForms)
                    {
                        ViewToProjectorMapping(form, deviceContext, userView, userFormProjection);
                    }

                    if (desktopDuplicationEnabled)
                        outputDuplication.ReleaseFrame();

                    //                    Console.WriteLine(stopwatch.ElapsedMilliseconds);
                    stopwatch.Restart();
                }
            }
        }

        private void CreateQuadsScaled()
        {
            var mesh = Mesh.FromOBJFile("Content/Quad.obj");
            meshDeviceResources = new MeshDeviceResources(device, imagingFactory, mesh);

            var worldScale = SharpDX.Matrix.Scaling(0.5f, 0.5f, 1f);

            {
                //Main quad for slides
                var scaling = SharpDX.Matrix.Scaling(1.5f, 0.75f, 1.0f);
                var translation = SharpDX.Matrix.Translation(0f, 0f, 2.8f);
                var world = worldScale * scaling * translation;
                var quad = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 5);
                //                quad.CalculateRotation(SharpDX.Matrix.Zero);
                quads.Add(quad);
            }


            {
                //Main quad for slides
                var scaling = SharpDX.Matrix.Scaling(1.5f, 0.75f, 1.0f);
                var translation = SharpDX.Matrix.Translation(1.6f, 0f, 2.8f);
                var world = worldScale * scaling * translation;
                var quad = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, desktopTextureSRV);
                //                quad.CalculateRotation(SharpDX.Matrix.Zero);
                quads.Add(quad);
            }

            {
                //                //Small quad holding skype
                var scaling = SharpDX.Matrix.Scaling(0.5f, 0.25f, 1.0f);
                var translation = SharpDX.Matrix.Translation(-0.8f, 0.52f, 2.8f);
                var world = worldScale * scaling * translation;

                var quad = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 4);

                quads.Add(quad);
            }

            {
                //Quad holding the cursor
                var scaling = SharpDX.Matrix.Scaling(1.0f, 0.5f, 1.0f) * SharpDX.Matrix.Scaling(0.2f);
                var translation = SharpDX.Matrix.Translation(0, 0, 2.7f);
                var world = worldScale * scaling * translation;

                cursor = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 2);
            }
        }

        private void CreateQuadsCylinder()
        {
            var mesh = Mesh.FromOBJFile("Content/Quad.obj");
            meshDeviceResources = new MeshDeviceResources(device, imagingFactory, mesh);

            var worldScale = SharpDX.Matrix.Scaling(0.5f, 0.5f, 1f);

            {
                //Main quad for slides
                var scaling = SharpDX.Matrix.Scaling(1.5f, 0.75f, 1.0f);
                var translation = SharpDX.Matrix.Translation(0f, 0f, 1.0f);
                var world = worldScale * scaling * translation;
                var quad = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 5);
                quad.CalculateRotation(SharpDX.Matrix.Zero);
                quads.Add(quad);
            }


            {
                //Main quad for slides
                var scaling = SharpDX.Matrix.Scaling(1.5f, 0.75f, 1.0f);
                var translation = SharpDX.Matrix.Translation(0f, 0f, 1.0f);
                var world = worldScale * scaling * translation;
                var quad = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 3);
                quad.CalculateRotation(SharpDX.Matrix.Translation(-2.0f, 0f, 0f));
                quads.Add(quad);
            }

            {
                //                //Small quad holding skype
                var scaling = SharpDX.Matrix.Scaling(0.5f, 0.25f, 1.0f);
                var translation = SharpDX.Matrix.Translation(0.0f, 0.5f, 1.0f);
                var world = worldScale * scaling * translation;
                var quad = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 4);
                quad.CalculateRotation(Matrix.Translation(-0.7f, 0, 0));
                quads.Add(quad);
            }

            {
                //Quad holding the cursor
                var scaling = SharpDX.Matrix.Scaling(1.0f, 0.5f, 1.0f) * SharpDX.Matrix.Scaling(0.1f);
                var translation = SharpDX.Matrix.Translation(0, 0, 0.9f);
                var world = worldScale * scaling * translation;

                cursor = new Quad(meshShader, meshDeviceResources, userViewRenderTargetView, userViewDepthStencilView, userViewViewport,
                    world, 2);
            }
        }

        private void SetupDesktopDuplicate()
        {
            //            windowPtr = ProjectionMappingSample.FindWindow(null, "profile.jpg - Windows Photo Viewer");
            windowPtr = ProjectionMappingSample.FindWindow(null, "small test video - VLC media player");

            // maybe use FindWindowEx to find child window

            var rect = new ProjectionMappingSample.RECT();
            ProjectionMappingSample.GetWindowRect(windowPtr, out rect);
            var desktopTextureDesc = new Texture2DDescription()
            {
                Width = (rect.Right + rightNudge) - (rect.Left + leftNudge),
                Height = (rect.Bottom + bottomNudge) - (rect.Top + topNudge),
                MipLevels = 1, // revisit this; we may benefit from mipmapping?
                ArraySize = 1,
                Format = SharpDX.DXGI.Format.B8G8R8A8_UNorm,
                SampleDescription = new SharpDX.DXGI.SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
            };
            desktopTexture = new Texture2D(device, desktopTextureDesc);
            desktopTextureSRV = new ShaderResourceView(device, desktopTexture);
        }

        private void SetupDesktopDuplicate2()
        {
            windowPtr2 = ProjectionMappingSample.FindWindow(null, "profile.jpg - Windows Photo Viewer");

            // maybe use FindWindowEx to find child window

            var rect = new ProjectionMappingSample.RECT();
            ProjectionMappingSample.GetWindowRect(windowPtr2, out rect);
            var desktopTextureDesc = new Texture2DDescription()
            {
                Width = (rect.Right + rightNudge) - (rect.Left + leftNudge),
                Height = (rect.Bottom + bottomNudge) - (rect.Top + topNudge),
                MipLevels = 1, // revisit this; we may benefit from mipmapping?
                ArraySize = 1,
                Format = SharpDX.DXGI.Format.B8G8R8A8_UNorm,
                SampleDescription = new SharpDX.DXGI.SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
            };
            desktopTexture2 = new Texture2D(device, desktopTextureDesc);
            desktopTextureSRV2 = new ShaderResourceView(device, desktopTexture2);
        }

        private void CreateUserDepth()
        {
            var userViewDepthBufferDesc = new Texture2DDescription()
            {
                Width = userViewTextureWidth,
                Height = userViewTextureHeight,
                MipLevels = 1,
                ArraySize = 1,
                Format = Format.D32_Float, // necessary?
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.DepthStencil,
                CpuAccessFlags = CpuAccessFlags.None
            };
            var userViewDepthStencil = new Texture2D(device, userViewDepthBufferDesc);
            userViewDepthStencilView = new DepthStencilView(device, userViewDepthStencil);
        }

        private void CreateTexture()
        {
            var textureDesc = new Texture2DDescription()
            {
                Width = 100,
                Height = 100,
                MipLevels = 1,
                ArraySize = 1,
                Format = SharpDX.DXGI.Format.R8G8B8A8_UNorm,
                SampleDescription = new SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None
            };
            var texture = new Texture2D(device, textureDesc);
        }

        private void CreateUserView()
        {
            var userViewTextureDesc = new Texture2DDescription()
            {
                Width = userViewTextureWidth,
                Height = userViewTextureHeight,
                MipLevels = 1, // revisit this; we may benefit from mipmapping?
                ArraySize = 1,
                Format = SharpDX.DXGI.Format.B8G8R8A8_UNorm,
                SampleDescription = new SharpDX.DXGI.SampleDescription(1, 0),
                Usage = ResourceUsage.Default,
                BindFlags = BindFlags.RenderTarget | BindFlags.ShaderResource,
                CpuAccessFlags = CpuAccessFlags.None,
            };
            var userViewRenderTarget = new Texture2D(device, userViewTextureDesc);
            userViewRenderTargetView = new RenderTargetView(device, userViewRenderTarget);
            userViewSRV = new ShaderResourceView(device, userViewRenderTarget);

            //            var filteredUserViewRenderTarget = new Texture2D(device, userViewTextureDesc);
            //            filteredUserViewRenderTargetView = new RenderTargetView(device, filteredUserViewRenderTarget);
            //            filteredUserViewSRV = new ShaderResourceView(device, filteredUserViewRenderTarget);
        }

        private void CreateShaders()
        {
            //            depthAndColorShader = new DepthAndColorShader(device);
            projectiveTexturingShader = new ProjectiveTexturingShader(device);
            passThroughShader = new PassThrough(device, userViewTextureWidth, userViewTextureHeight);
            //            radialWobbleShader = new RadialWobble(device, userViewTextureWidth, userViewTextureHeight);
            meshShader = new MeshShader(device);
            fromUIntPS = new FromUIntPS(device, Kinect2Calibration.depthImageWidth, Kinect2Calibration.depthImageHeight);
            bilateralFilter = new BilateralFilter(device, Kinect2Calibration.depthImageWidth,
                Kinect2Calibration.depthImageHeight);
        }

        private void SmoothDepthImage(ProjectorCameraEnsemble.Camera camera, DeviceContext deviceContext)
        {
            var cameraDeviceResource = cameraDeviceResources[camera];
            if (cameraDeviceResource.depthImageChanged)
            {
                fromUIntPS.Render(deviceContext, cameraDeviceResource.depthImageTextureRV,
                    cameraDeviceResource.floatDepthImageRenderTargetView);
                for (int i = 0; i < 1; i++)
                {
                    bilateralFilter.Render(deviceContext, cameraDeviceResource.floatDepthImageRV,
                        cameraDeviceResource.floatDepthImageRenderTargetView2);
                    bilateralFilter.Render(deviceContext, cameraDeviceResource.floatDepthImageRV2,
                        cameraDeviceResource.floatDepthImageRenderTargetView);
                }
                cameraDeviceResource.depthImageChanged = false;
            }
        }

        private void ViewToProjectorMapping(ProjectorForm form, DeviceContext deviceContext, Matrix userView,
            Matrix userProjection)
        {
            deviceContext.ClearRenderTargetView(form.renderTargetView, new Color4(0f, 0f, 0f, 0f)); // Color of the edges from edge detection
            deviceContext.ClearDepthStencilView(form.depthStencilView, DepthStencilClearFlags.Depth, 1, 0);

            foreach (var camera in ensemble.cameras)
            {
                var cameraDeviceResource = cameraDeviceResources[camera];

                var world = new SharpDX.Matrix();
                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 4; j++)
                        world[i, j] = (float)camera.pose[i, j];
                world.Transpose();

                var projectorWorldViewProjection = world * form.view * form.projection;
                var userWorldViewProjection = world * userView * userProjection;

                projectiveTexturingShader.SetConstants(deviceContext, userWorldViewProjection, projectorWorldViewProjection);

                // TODO: clean this up by simply using a pointer to the userViewSRV
                if (threeDObjectEnabled)
                    projectiveTexturingShader.Render(deviceContext, cameraDeviceResource.floatDepthImageRV, userViewSRV, cameraDeviceResource.vertexBuffer, form.renderTargetView, form.depthStencilView, form.viewport);
            }

            form.swapChain.Present(1, PresentFlags.None);

        }

        private Vector3 TrackedHeadPosition()
        {
            Vector3 headPosition;
            float distanceSquared = 0;
            lock (headCameraSpacePointLock)
            {
                headPosition = new SharpDX.Vector3(headCameraSpacePoint.X, headCameraSpacePoint.Y, headCameraSpacePoint.Z);

                float dx = handLeftCameraSpacePoint.X - handRightCameraSpacePoint.X;
                float dy = handLeftCameraSpacePoint.Y - handRightCameraSpacePoint.Y;
                float dz = handLeftCameraSpacePoint.Z - handRightCameraSpacePoint.Z;
                distanceSquared = dx * dx + dy * dy + dz * dz;
            }
            var rotationY = SharpDX.Matrix.RotationY((float)Math.PI);
            var translation = SharpDX.Matrix.Translation(headPositionDefault.X, headPositionDefault.Y, 0);
            var transform = rotationY * translation;
            var headPositionTrans = SharpDX.Vector3.TransformCoordinate(headPosition, transform);

            Console.WriteLine("X: {0},\t Y: {1}", headPosition.X, headPositionTrans.Y);

            if (trackingValid && (distanceSquared < 0.02f) && (alpha > 1))
            {
                alpha = 0;
            }
            return headPositionTrans;
        }

        private Vector3 TrackedHandPosition()
        {
            Vector3 handRightPosition;

            lock (headCameraSpacePointLock)
            {
                handRightPosition = new SharpDX.Vector3(handRightCameraSpacePoint.X, handRightCameraSpacePoint.Y, handRightCameraSpacePoint.Z);

                if (handLeftCameraSpacePoint.Y > elbowLeftCameraSpacePoint.Y)
                {
                    debug = 1;
                }
                else
                {
                    debug = 0;
                }
            }


            var rotationY = SharpDX.Matrix.RotationY((float)Math.PI);
            var translation = SharpDX.Matrix.Translation(handRightPositionDefault.X, handRightPositionDefault.Y, 2.8f);
            var transform = rotationY * translation;

            var handRightPositionTransform = SharpDX.Vector3.TransformCoordinate(handRightPosition, transform);

            //            Console.WriteLine("HandpositionTrans: " + handRightPositionTransform);

            //            Console.WriteLine("Raw: {0}, \t Trans: {1}", handRightPosition.X, handRightPositionTransform.X);
            return handRightPositionTransform;
        }

        void LocalBodyLoop()
        {
            while (true)
            {
                // find closest tracked head
                var bodyFrame = bodyFrameReader.AcquireLatestFrame();

                if (bodyFrame != null)
                {
                    if (bodies == null)
                        bodies = new Body[bodyFrame.BodyCount];
                    bodyFrame.GetAndRefreshBodyData(bodies);

                    bool foundTrackedBody = false;
                    float distanceToNearest = float.MaxValue;
                    var nearestHeadCameraSpacePoint = new CameraSpacePoint();
                    var nearestHandRightCameraSpacePoint = new CameraSpacePoint();
                    var nearestHandLeftCameraSpacePoint = new CameraSpacePoint();

                    var nearestHandTipLeftCameraSpacePoint = new CameraSpacePoint();
                    var nearestHandTipRightCameraSpacePoint = new CameraSpacePoint();
                    var nearestThumbLeftCameraSpacePoint = new CameraSpacePoint();
                    var nearestThumbRightCameraSpacePoint = new CameraSpacePoint();
                    var nearestElbowLeftCameraSpacePoint = new CameraSpacePoint();
                    var nearestElbowRightCameraSpacePoint = new CameraSpacePoint();

                    foreach (var body in bodies)
                        if (body.IsTracked)
                        {
                            var cameraSpacePoint = body.Joints[JointType.Head].Position;
                            if (cameraSpacePoint.Z < distanceToNearest)
                            {
                                distanceToNearest = cameraSpacePoint.Z;
                                nearestHeadCameraSpacePoint = cameraSpacePoint;
                                nearestHandLeftCameraSpacePoint = body.Joints[JointType.HandLeft].Position;
                                nearestHandRightCameraSpacePoint = body.Joints[JointType.HandRight].Position;

                                nearestHandTipLeftCameraSpacePoint = body.Joints[JointType.HandTipLeft].Position;
                                nearestHandTipRightCameraSpacePoint = body.Joints[JointType.HandTipRight].Position;
                                nearestThumbLeftCameraSpacePoint = body.Joints[JointType.ThumbLeft].Position;
                                nearestThumbRightCameraSpacePoint = body.Joints[JointType.ThumbRight].Position;
                                nearestElbowLeftCameraSpacePoint = body.Joints[JointType.ElbowLeft].Position;
                                nearestElbowRightCameraSpacePoint = body.Joints[JointType.ElbowRight].Position;
                                foundTrackedBody = true;
                            }
                        }

                    lock (headCameraSpacePointLock)
                    {
                        if (foundTrackedBody)
                        {
                            headCameraSpacePoint = nearestHeadCameraSpacePoint;
                            handLeftCameraSpacePoint = nearestHandLeftCameraSpacePoint;
                            handRightCameraSpacePoint = nearestHandRightCameraSpacePoint;

                            handLeftTipCameraSpacePoint = nearestHandTipLeftCameraSpacePoint;
                            handRightTipCameraSpacePoint = nearestHandTipRightCameraSpacePoint;
                            thumbLeftCameraSpacePoint = nearestThumbLeftCameraSpacePoint;
                            thumbRightCameraSpacePoint = nearestThumbRightCameraSpacePoint;
                            elbowLeftCameraSpacePoint = nearestElbowLeftCameraSpacePoint;
                            elbowRightCameraSpacePoint = nearestElbowRightCameraSpacePoint;

                            trackingValid = true;
                            //                            Console.WriteLine("{0} {1} {2}", headCameraSpacePoint.X, headCameraSpacePoint.Y, headCameraSpacePoint.Z);
                        }
                        else
                        {
                            headCameraSpacePoint.X = 0f;
                            headCameraSpacePoint.Y = 0.1f;
                            headCameraSpacePoint.Z = 1.5f;

                            trackingValid = false;
                        }
                    }

                    bodyFrame.Dispose();
                }
                else
                    System.Threading.Thread.Sleep(5);
            }
        }
    }
}
